package dataLayer;

import java.io.*;
import java.nio.file.*;
import java.text.*;
import java.util.*;
import java.util.Map.*;
import java.util.stream.*;



public class MainController { 
	
    private ArrayList<Event> events = new ArrayList<Event>();
    private static final String fileName = "activities.txt";
    private static final String outputFileName = "applicationOutput.txt";
    private static final String delimiter = "---------------------------------------------------------------------------------";
    public void createList() {
    	try {
    		Stream<String> stream = Files.lines(Paths.get(MainController.fileName)); 
    		List<String> allEvents = (List<String>) stream.map(String::toLowerCase).collect(Collectors.toList());
    		SimpleDateFormat pattern = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    		for(String s : allEvents) {
    			String[] details = s.split("\\s+"); 
    			String startingTime = details[0] + " " + details[1]; 
    			String finishingTime = details[2] + " " + details[3]; 
    			String name = details[4]; 
    			this.events.add(new Event(pattern.parse(startingTime),pattern.parse(finishingTime), name));
    		} 
    		stream.close();
    	} 
    	catch(IOException ex) {
    		System.out.println(ex.getMessage());
    	} 
    	catch(ParseException ex) {
    		System.out.println(ex.getMessage());
    	}   
    	
    }  
    
    public int countDays() {  
    	HashSet<String> list = new HashSet<String>(); 
    	for(Event e : this.events) {
    		String[] data = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(e.getStartingTime()).split(" "); 
    		list.add(data[0]); 
    	} 
    	return list.size();
    }
    
    public Map<String, Long> countActivities(){
    	return this.events.stream().collect(Collectors.groupingBy(Event::getName,Collectors.counting()));
    } 
    
    public Map<Integer,Map<String,Long>> countActivitiesPerDay(){
    	return this.events.stream().collect(Collectors.groupingBy(Event::getDate,Collectors.groupingBy(Event::getName,Collectors.counting())));
    } 
    
    public List<String> getDuration(){
    	List<String> list = new ArrayList<String>(); 
    	for(Event e : this.events){
    		list.add(e.getDurations().toString());
    	} 
    	return list;
    } 
    
    public Map<String, Long> getDurationActivity(){
    	return this.events.stream().collect(Collectors.groupingBy(Event::getName,Collectors.summingLong(e -> e.getFinishingTime().getTime() - e.getStartingTime().getTime())));
    } 
    
    public void writeResults() {
    	try {  
    		File f = new File(MainController.outputFileName);  
    		if(f.exists()) {
    			f.delete(); 
    			f.createNewFile();
    		}
    		BufferedWriter buff = new BufferedWriter(new FileWriter(MainController.outputFileName,true)); 
    		ArrayList<String> output = new ArrayList<String>();   
    		
    		this.createList();
    		output.add("The list has been created!"); 
    		output.add(MainController.delimiter);
    		output.add("1.The number of monitored days is "+this.countDays()); 
    		output.add(MainController.delimiter); 
    		output.add("2.The frequency of the the activities over the whole monitoring period is :");
    		Map<String , Long> map = this.countActivities(); 
        	for(Entry<String, Long> entry : map.entrySet()) 
        		output.add(entry.getKey() + " - " + entry.getValue()); 
        	output.add(MainController.delimiter);  
        	output.add("3.The number of times each activity has appeared each day over the monitoring period is :"); 
        	Map<Integer, Map<String,Long>> map2 = this.countActivitiesPerDay();
        	for(Entry<Integer , Map<String,Long>> entry : map2.entrySet()) {
        		int day = entry.getKey(); 
        		Map<String,Long> activity = entry.getValue();  
        		output.add("At " + day + ":\n");
        		for(Entry<String , Long> entry2 : activity.entrySet()) {
        			String name = entry2.getKey(); 
        			long counter = entry2.getValue();  
        			output.add(name + " - " + counter); 
        		} 
        	} 
        	output.add(MainController.delimiter); 
        	output.add("4.The duration of each activity is :"); 
        	List<String> list = this.getDuration();
        	for(int i=0;i<this.events.size();i++)  
        		output.add(this.events.get(i).getName()+" - "+list.get(i)); 
        	output.add(MainController.delimiter); 
        	output.add("5.The overall duration of each activity is :"); 
        	Map<String,Long> map3 = this.getDurationActivity(); 
        	for(Entry<String,Long> entry : map3.entrySet()) {
        		output.add(entry.getKey()+" - "+ (new TimeData(entry.getValue()).toString()));
        	}
        	
        	for(String s : output) {
        		buff.append(s); 
        		buff.newLine();
        	}
        	
        	buff.close();
    		
    	} 
    	catch(IOException ex) {
    		System.out.println(ex.getMessage());
    	}
    }
    
    public static void main(String[] args) {
    	MainController c = new MainController();  
    	c.writeResults();
    	
    }
    
   
   
}
