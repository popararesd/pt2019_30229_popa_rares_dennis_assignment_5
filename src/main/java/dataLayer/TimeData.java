package dataLayer;

import java.util.concurrent.TimeUnit;

public class TimeData {
	private long seconds; 
	private long minutes; 
	private long hours; 
	private long days;
	
	public TimeData(long millis) {
		   long days = TimeUnit.MILLISECONDS.toDays(millis); 
		   long hours = TimeUnit.MILLISECONDS.toHours(millis - TimeUnit.DAYS.toMillis(days)); 
		   long minutes = TimeUnit.MILLISECONDS.toMinutes(millis - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours)); 
		   long seconds = TimeUnit.MILLISECONDS.toSeconds(millis - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
		   this.days = days; 
		   this.hours = hours; 
		   this.minutes = minutes; 
		   this.seconds = seconds;
	}
	
	public TimeData(long seconds, long minutes, long hours, long days) {
		super();
		this.seconds = seconds;
		this.minutes = minutes;
		this.hours = hours;
		this.days = days;
	}

	public long getSeconds() {
		return seconds;
	}

	public void setSeconds(long seconds) {
		this.seconds = seconds;
	}

	public long getMinutes() {
		return minutes;
	}

	public void setMinutes(long minutes) {
		this.minutes = minutes;
	}

	public long getHours() {
		return hours;
	}

	public void setHours(long hours) {
		this.hours = hours;
	}

	public long getDays() {
		return days;
	}

	public void setDays(long days) {
		this.days = days;
	}  
	
	public String toString() {
		return this.days+"d "+this.hours+"h "+this.minutes+"m "+this.seconds+"s";
	} 
	
	
	
}
