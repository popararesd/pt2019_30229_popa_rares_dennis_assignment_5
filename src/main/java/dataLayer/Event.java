package dataLayer;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Event  implements Comparable<Event>{

    private Date startingTime;
    private Date finishingTime;
    private String name;

   
    public  Event (Date startingTime , Date finishingTime , String name){
        this.startingTime = startingTime;
        this.finishingTime = finishingTime;
        this.name = name;
    }

    public Date getStartingTime(){
        return this.startingTime;
    } 
    
    public Date getFinishingTime() {
    	return this.finishingTime;
    }

   public String getName () {
	   return this.name;
   }
   
   @SuppressWarnings("deprecation")
public int getDate() {
	   return this.startingTime.getDate();
   } 
   @SuppressWarnings("deprecation")
public int getMonth() {
	   return  this.startingTime.getMonth();
   }
   
   public TimeData getDurations() {
	   long duration = this.finishingTime.getTime() - this.startingTime.getTime();  
	   long days = TimeUnit.MILLISECONDS.toDays(duration); 
	   long hours = TimeUnit.MILLISECONDS.toHours(duration - TimeUnit.DAYS.toMillis(days)); 
	   long minutes = TimeUnit.MILLISECONDS.toMinutes(duration - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours)); 
	   long seconds = TimeUnit.MILLISECONDS.toSeconds(duration - TimeUnit.DAYS.toMillis(days) - TimeUnit.HOURS.toMillis(hours) - TimeUnit.MINUTES.toMillis(minutes));
	   return new TimeData(seconds, minutes, hours, days);
   }
   
    public int compareTo(Event event){
        int ok = 0;
        if(this.startingTime.before(event.getStartingTime()))
            ok=1;
        else
            if(this.startingTime.after(event.getStartingTime()))
                ok=-1;
        return ok;
    }


}
